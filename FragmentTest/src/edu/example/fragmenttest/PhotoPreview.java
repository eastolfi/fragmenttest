package edu.example.fragmenttest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class PhotoPreview extends Activity {
	PhotoPreview mParent;
	ImageView img;
	LinearLayout layoutOptions;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_preview);
		
		mParent = this;
		
		layoutOptions = (LinearLayout) findViewById(R.id.layoutOptions);
		
		Button bt = (Button) findViewById(R.id.button1);
		bt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ExpandAnimation eAnim = new ExpandAnimation(layoutOptions, 500, ExpandAnimation.ANIM_TOP_BOTTOM);
				layoutOptions.startAnimation(eAnim);
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.photo_preview, menu);
		return true;
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}

}
