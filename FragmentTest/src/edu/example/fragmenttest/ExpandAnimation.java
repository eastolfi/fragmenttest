package edu.example.fragmenttest;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class ExpandAnimation extends Animation {
	public static int ANIM_TOP_BOTTOM = 0;
	public static int ANIM_BOTTOM_TOP = 1;
	public static int ANIM_LEFT_RIGHT = 2;
	public static int ANIM_RIGHT_LEFT = 3;
	/************/
	private View mAnimatedView;
    private LinearLayout.LayoutParams mViewLayoutParams;
    private int mMarginStart, mMarginEnd;
    private boolean mIsVisibleAfter = false;
    private boolean mWasEndedAlready = false;
    
    public ExpandAnimation(View v, int duration, int direction) {
    	// Se asigna la duracion de la animacion
    	setDuration(duration);
    	
    	// Se duplica la vista a animar con la que se trabajara luego
    	mAnimatedView = v;
    	
    	// Se recogen los parametros de la vista
    	mViewLayoutParams = (LayoutParams) v.getLayoutParams();

    	// Se guarda la visibilidad de la vista
    	mIsVisibleAfter = (v.getVisibility() == View.VISIBLE);
    	
    	// Se recogen los valores del margen
    	mMarginStart = mViewLayoutParams.bottomMargin;
    	mMarginEnd = (mMarginStart == 0 ? (0 - v.getHeight()) : 0);
    	
    	// Se muestra la vista	TODO: cambiar a mostrar / ocultar
    	v.setVisibility(View.VISIBLE);
    }

	@Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        super.applyTransformation(interpolatedTime, t);
 
        if (interpolatedTime < 1.0f) {
 
            // Calculating the new bottom margin, and setting it
            mViewLayoutParams.bottomMargin = mMarginStart + (int) ((mMarginEnd - mMarginStart) * interpolatedTime);
 
            // Invalidating the layout, making us seeing the changes we made
            mAnimatedView.requestLayout();
        }
        else if (!mWasEndedAlready) {
        	mViewLayoutParams.bottomMargin = mMarginEnd;
        	mAnimatedView.requestLayout();
        	
        	if (mIsVisibleAfter) {
        		mAnimatedView.setVisibility(View.GONE);
        	}
        	mWasEndedAlready = true;
        }
    }
}