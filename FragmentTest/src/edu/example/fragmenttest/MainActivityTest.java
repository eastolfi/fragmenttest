package edu.example.fragmenttest;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivityTest extends FragmentActivity {
	ImageView btCamera;
	Context ctx;
	Rect fotoButtonBounds;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_activity_test);
		startActivity(new Intent(this, CameraActivity.class));
		
		ctx = this;
		
		
		btCamera = (ImageView) findViewById(R.id.imgBtnCamera);
		btCamera.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					btCamera.setImageResource(R.drawable.camera_open);
				}
				else if (event.getAction() == MotionEvent.ACTION_UP) {
					if (isPosInBounds(v, event)) {
						btCamera.setImageResource(R.drawable.camera_closed);
						Intent i = new Intent(ctx, CameraActivity.class);
						startActivity(i);
					}
				}
				return true;
			}
		});
		
		Button bt = (Button) findViewById(R.id.button1);
		bt.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ctx, PhotoPreview.class);
				startActivity(i);
			}
		});
	}

	private void defineBounds(View v) {
		fotoButtonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	}
	private Boolean isPosInBounds(View v, MotionEvent event) {
		if (fotoButtonBounds.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_activity_test, menu);
		return true;
	}

}
