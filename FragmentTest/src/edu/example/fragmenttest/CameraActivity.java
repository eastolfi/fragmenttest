package edu.example.fragmenttest;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.os.Bundle;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class CameraActivity extends Activity {
	public ImageView mPictureTaken;
	LinearLayout layoutOptions;
	Context ctx;
	CameraActivity mParent;
	Camera mCamera;
	CameraPreview cameraPreview;
	Integer cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
	FrameLayout preview;
//	SeekBar zoomBar;
	ImageView btSwichCamera;
	ImageButton btCameraOptions;
	int currentZoomLevel = 0, maxZoomLevel = 0;
	Rect fotoButtonBounds;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_camera2);
		
		ctx = this;
		mParent = this;
		
		mPictureTaken = (ImageView) findViewById(R.id.pictureTaken);
		
//		Button btFocus = (Button) findViewById(R.id.btFocus);
//		btFocus.setOnClickListener(new OnClickListener() {
//			@Override
//			public void onClick(View v) {
//				mCamera.autoFocus(null);
//			}
//		});
		
		btSwichCamera = (ImageView) findViewById(R.id.btSwichCamera);
		btSwichCamera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (cameraId == Camera.CameraInfo.CAMERA_FACING_BACK) {
					cameraId = Camera.CameraInfo.CAMERA_FACING_FRONT;
				}
				else if (cameraId == Camera.CameraInfo.CAMERA_FACING_FRONT) {
					cameraId = Camera.CameraInfo.CAMERA_FACING_BACK;
				}
				mCamera = openCamera(cameraId);
			}
		});
		
		ImageButton btFoto = (ImageButton) findViewById(R.id.btFoto);
		btFoto.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_DOWN) {
					defineBounds(v);
					mCamera.autoFocus(null);
				}
				else if (event.getAction() == MotionEvent.ACTION_UP) {
					if (isPosInBounds(v, event)) {
						mCamera.cancelAutoFocus();
						makePhoto();
					}
				}
				return true;
			}
		});
		layoutOptions = (LinearLayout) findViewById(R.id.layoutOptions);
		btCameraOptions = (ImageButton) findViewById(R.id.btCameraOptions);
		btCameraOptions.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (layoutOptions.getVisibility() == View.GONE) {
					layoutOptions.setVisibility(View.VISIBLE);
					layoutOptions.bringToFront();
				}
				else {
					layoutOptions.setVisibility(View.GONE);
				}
			}
		});
		/*** Efectos ***/
		Button btEffect1 = (Button) findViewById(R.id.btEffect1);
		Button btEffect2 = (Button) findViewById(R.id.btEffect2);
		Button btEffect3 = (Button) findViewById(R.id.btEffect3);
		Button btEffect4 = (Button) findViewById(R.id.btEffect4);
		Button btEffect5 = (Button) findViewById(R.id.btEffect5);
		
		View.OnClickListener cl = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Parameters p = mCamera.getParameters();
				switch (v.getId()) {
				case R.id.btEffect1:
					p.setColorEffect(Camera.Parameters.EFFECT_AQUA);
					break;
				case R.id.btEffect2:
					p.setColorEffect(Camera.Parameters.EFFECT_NEGATIVE);
					break;
				case R.id.btEffect3:
					p.setColorEffect(Camera.Parameters.EFFECT_SEPIA);
					break;
				case R.id.btEffect4:
					p.setColorEffect(Camera.Parameters.EFFECT_MONO);
					break;
				case R.id.btEffect5:
					p.setColorEffect(Camera.Parameters.EFFECT_NONE);
					break;
				}
				mCamera.setParameters(p);
			}
		};
		
		btEffect1.setOnClickListener(cl);
		btEffect2.setOnClickListener(cl);
		btEffect3.setOnClickListener(cl);
		btEffect4.setOnClickListener(cl);
		btEffect5.setOnClickListener(cl);
		
		/***************/
		
		ImageButton btFlash = (ImageButton) findViewById(R.id.btFlash);
		btFlash.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Parameters p = mCamera.getParameters();
				if (p.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)) {
					p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
				}
				else {
					p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
				}
				mCamera.setParameters(p);
			}
		});
		
		mCamera = openCamera(cameraId);
//		zoomBar = (SeekBar) findViewById(R.id.zoomBar);
//		if (mCamera.getParameters().isZoomSupported()) {
//			zoomBar.setMax(mCamera.getParameters().getMaxZoom());
//			zoomBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
//				@Override
//				public void onStopTrackingTouch(SeekBar seekBar) {
//					Log.d("", "");
//				}
//				
//				@Override
//				public void onStartTrackingTouch(SeekBar seekBar) {
//					Log.d("", "");
//				}
//				
//				@Override
//				public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//	//				mCamera.startSmoothZoom(progress);
//					Parameters p = mCamera.getParameters();
//					p.setZoom(progress);
//					mCamera.setParameters(p);
//				}
//			});
//		}
//		else {
//			zoomBar.setVisibility(View.GONE);
//		}
	}
	
	private void defineBounds(View v) {
		fotoButtonBounds = new Rect(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
	}
	private Boolean isPosInBounds(View v, MotionEvent event) {
		if (fotoButtonBounds.contains(v.getLeft() + (int) event.getX(), v.getTop() + (int) event.getY())) {
			return true;
		}
		return false;
	}
	
	private boolean hasCamera(Context context) {
		if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private Camera openCamera(Integer cameraNumber) {
		Camera c = mCamera;
//		if (mCamera != null) c = mCamera;
		if (hasCamera(ctx)) {
			try {
				if (c != null) {
					c.stopPreview();
					c.release();
					c = null;
				}
				if (cameraNumber != null) {
					c = Camera.open(cameraNumber);
				}
				else {
					c = Camera.open();
				}
				Camera.Parameters params = c.getParameters();
				params.setPictureSize(600, 480);
				c.setParameters(params);
				preview = (FrameLayout) findViewById(R.id.cameraPreview);
				if (cameraPreview != null) {
					preview.removeView(cameraPreview);
				}
				cameraPreview = new CameraPreview(ctx, c);
				preview.addView(cameraPreview);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		prepareScreen();
		
		return c;
	}
	
	private void makePhoto() {
		if (mCamera != null) {
			MPictureCallbacks pictureJpgCB = new MPictureCallbacks(mParent);
			mCamera.takePicture(null, null, pictureJpgCB);
		}
	}
	
	private void prepareScreen() {
		//Comprobar zoom
		//Comprobar focus
		//Mover elementos al frente
		btSwichCamera.bringToFront();
		btCameraOptions.bringToFront();
	}
	
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.camera, menu);
		return true;
	}

	@Override
	protected void onPause() {
//		mCamera.unlock();
		if (mCamera != null) {
			try {mCamera.stopPreview();}catch (Exception e) {}
			mCamera.release();
			mCamera = null;
		}
		super.onPause();
	}

	@Override
	protected void onDestroy() {
//		mCamera.release();
		super.onDestroy();
	}
	
	@Override
	protected void onResume() {
		if (mCamera != null) {
			try {
				mCamera.reconnect();
			} catch (Exception e) {
				openCamera(cameraId);
			}
		}
		else {
			openCamera(cameraId);
		}
		super.onResume();
	}
	
}
