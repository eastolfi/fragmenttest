package edu.example.fragmenttest;

import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.sax.StartElementListener;
import android.util.Log;
import android.widget.ImageView;

public class MPictureCallbacks implements PictureCallback {
	CameraActivity parent;
	
	public MPictureCallbacks(CameraActivity parent) {
		this.parent = parent;
	}
	
	@Override
	public void onPictureTaken(byte[] data, Camera camera) {
		Log.d("", "");
//		parent.mCamera.release();
		AlertDialog.Builder builder = new AlertDialog.Builder(parent);
		ImageView img = new ImageView(parent);
		Bitmap photo = BitmapFactory.decodeByteArray(data, 0, data.length);
//		photo = Bitmap.createScaledBitmap(photo, 50, 50, false);
		Matrix matrix = new Matrix();
		matrix.postRotate(90);
		int nW = photo.getWidth();
		int nH = photo.getHeight();
		photo = Bitmap.createBitmap(photo, 0, 0, nW, nH, matrix, true);
		
		img.setImageBitmap(photo);
		builder.setView(img);
		AlertDialog dialog = builder.create();
		dialog.show();
		
//		Intent i = new Intent(parent, PhotoPreview.class);
//		i.putExtra("bytePhoto", data);
//		parent.startActivity(i);
//		parent.mPictureTaken.setImageBitmap(photo);
		
	}

}
